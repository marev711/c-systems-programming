#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>

char* type2str(char type) {
#ifdef _DIRENT_HAVE_D_TYPE
    switch (type) {
        case DT_BLK:
            return "Block device";
        case DT_CHR:
            return "Char device";
        case DT_DIR:
            return "Directory";
        case DT_FIFO:
            return "Fifo";
        case DT_LNK:
            return "Sym. link";
        case DT_REG:
            return "Normal file";
        case DT_SOCK:
            return "Socket";
        case DT_UNKNOWN:
        default:
            return "Unknown";
    }
#else
    return "DT_* not supported";
#endif
}

int isDir(char type) {
#ifdef _DIRENT_HAVE_D_TYPE
    return type == DT_DIR;
#else
    return 0;
#endif
}

void listDir(char* path) {
    DIR* dir = opendir(path);
    if (dir == NULL) {
        perror("myls");
        exit(1);
    }

    printf("Listing content of %s\n", path);
    struct dirent* entry;
    while ((entry = readdir(dir)) != NULL) {
        struct stat info;
        lstat(entry->d_name, &info);

        const int N = 128;
        char mtime[N];
        strftime(mtime, N, "%F %T", localtime(&info.st_mtime));

        printf(" %20s%c %-12s %8ld bytes [%s] (links=%d)\n",
               entry->d_name,
               isDir(entry->d_type) ? '/' : ' ',
               type2str(entry->d_type),
               info.st_size,
               mtime,
               (int) info.st_nlink
        );
    }

    closedir(dir);
}

int main(int numArgs, char* args[]) {
    if (numArgs == 1) {
        listDir(".");
    } else {
        for (int k = 1; k < numArgs; ++k)
            listDir(args[k]);
    }

    return 0;
}

