#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>


void runPS(const char* filename) {
    int fd = open(filename, O_WRONLY | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR);
    if (fd < 0) {
        fprintf(stderr, "failed open: %s\n", strerror(errno));
        exit(1);
    }

    int rc = dup2(fd, STDOUT_FILENO);
    if (rc < 0) {
        fprintf(stderr, "failed dup2: %s\n", strerror(errno));
        exit(1);
    }
    close(fd);

    execl("/bin/ps", "ps", "-ef", NULL);
    fprintf(stderr, "failed exec: %s\n", strerror(errno));
    exit(1);
}

int main() {
    const char* filename = "./ps-output.txt";

    if (fork() == 0) {
        runPS(filename);
    }

    int status = 0;
    wait(&status);
    printf("output file: %s\n", filename);

    return 0;
}
