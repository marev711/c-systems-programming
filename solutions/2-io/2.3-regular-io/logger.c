//COMPILE: c99 -g -Wall logger.c -o logger
//RUN    : ./logger <num-writes> <message> <logfile>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


char* now() {
    static char buf[128];
    memset(buf, '\0', sizeof(buf));
    time_t now = time(NULL);
    struct tm* tmp = localtime(&now);
    strftime(buf, sizeof(buf), "%F %T", tmp);
    return buf;
}

int main(int numArgs, char* args[]) {
    int numMsgs = 1000000;
    char* msg = "Linux is a very cool operating system!!";
    char* filename = "./logger.log";

    if (numArgs > 1) numMsgs = atoi(args[1]);
    if (numArgs > 2) msg = args[2];
    if (numArgs > 3) filename = args[3];

    const int MAXLINE = 1024;
    if (strlen(msg) > (MAXLINE - 2)) {
        fprintf(stderr, "Too long message\n");
        exit(1);
    }

    int log = open(filename,
                   O_WRONLY | O_CREAT | O_APPEND,
                   S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);  //rw-r--r--
    if (log < 0) {
        perror("open");
        exit(1);
    }
    printf("logger[%d] logfile='%s', # msgs=%d\n", getpid(), filename, numMsgs);

    size_t numBytes = 0;
    int numRemaining = numMsgs;
    char logline[MAXLINE];
    do {
        sprintf(logline, "[%d] %s %s\n", getpid(), now(), msg);
        numBytes += write(log, logline, strlen(logline));
    } while (numRemaining-- > 0);

    close(log);

    printf("--- %s ---\n  %d messages\n  %d bytes/message\n  %d bytes total\n",
           filename, numMsgs, (int) strlen(msg), (int) numBytes);

    return 0;
}

