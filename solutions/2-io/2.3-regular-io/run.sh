#!/usr/bin/env bash
set -e

LOGGERS=10
MESSAGES=1000000
LOGFILE=logger.log

rm -f $LOGFILE
touch $LOGFILE
echo "Launching $LOGGERS loggers..."
for n in `seq 1 $LOGGERS`
do
    echo "Starting logger no. $n"
    ./cmake-build-debug/logger $MESSAGES 'Foobar strikes again and again' $LOGFILE &
done
#tail -f $LOGFILE
