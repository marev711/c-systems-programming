#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// -------------------------
// --- Account
// -------------------------
typedef struct {
    int     balance;
    float   rate;
} Account;

Account*  account_init(Account* this, int balance, float rate) {
    this->balance = balance;
    this->rate    = rate;
    return this;
}

void account_print(Account* this) {
    printf("Account{SEK %d, %1f%%} @ %15ld\n",
        this->balance, this->rate, (unsigned long)this);
}


// -------------------------
// --- App
// -------------------------
Account  global;

void func() {
    printf(">> func()\n");

    Account     local;
    account_init(&local, 400, 5);
    printf("STACK: "); account_print(&local);

    printf("<< func()\n");
}

int main() {
    printf("page size = %d bytes\n", getpagesize());

    account_init(&global, 100, 2);
    printf("DATA : "); account_print(&global);

    Account     local;
    account_init(&local, 200, 3);
    printf("STACK: "); account_print(&local);

    Account*    heap = (Account*)calloc(1, sizeof(Account));
    account_init(heap  , 300, 4);
    printf("HEAP : "); account_print(heap);

    func();

    free(heap);

    return 0;
}
