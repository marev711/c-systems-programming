#!/bin/bash
set -e
set -x

gcc -std=c99 -Wall -Wno-unused-variable -fmax-errors=1 -save-temps -time hello.c -o hello-dyn
gcc -std=c99 -Wall -Wno-unused-variable  -fmax-errors=1 -save-temps -time --static hello.c -o hello-sta

