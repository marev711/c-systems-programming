//COMPILE: c99 -Wall -g longjmp.c -o longjmp
//RUN    : ./longjmp

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>

static jmp_buf exception;
#define    ERROR  1

int factorial(int n) {
    static int indent = 1;
    const int TAB = 2;

    printf(">> %*c[factorial(%d)]\n", indent, ' ', n);
    indent += TAB;

    if (n == 5) {
        printf("   %*c[factorial(%d)] JUMP\n", indent, ' ', n);
        longjmp(exception, ERROR);
    }

    int result = (n <= 1) ? 1 : n * factorial(n - 1);

    indent -= TAB;
    printf("<< %*c[factorial(%d)] = %d\n", indent, ' ', n, result);
    return result;
}

void compute(int n) {
    printf(">> [compute(%d)]\n", n);
    char* blk = malloc(42);
    //C++: unique_ptr<char*> ptr = { malloc(42) };

    int result = factorial(n);

    free(blk);
    printf("<< [compute(%d)] --> %d\n", n, result);
}

int main() {
    printf(">> [main]\n");

    int rc = setjmp(exception);
    if (rc < 0) return 1;
    if (rc == 0) {
        compute(4);
        printf("   [main] ----------------------\n");
        compute(8);
    } else
        switch (rc) {
            case ERROR:
                printf("   [main] Received an exception\n");
                break;
            default:
                printf("   [main] Unknown EXCEPTION");
                exit(2);
                break;
        }

    printf("<< [main]\n");
    return 0;
}

