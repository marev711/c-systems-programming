cmake_minimum_required(VERSION 3.7)
project(3_message_queues)

set(CMAKE_C_STANDARD 99)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}  -Wall -Wextra -Wpedantic -Werror -Wfatal-errors -Wno-unused-parameter")

add_executable(fibsrv
        intq.h intq.c
        fibsrv.c)
target_link_libraries(fibsrv rt)


add_executable(fibcli
        intq.h intq.c
        fibcli.c)
target_link_libraries(fibcli rt)

