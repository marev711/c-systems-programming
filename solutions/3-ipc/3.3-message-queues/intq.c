//COMPILE: c99 -c intq.c

#include "intq.h"

static void fail(const char* msg) {
    fprintf(stderr, "ERROR: %s\n", msg);
    perror("MQ");
    exit(1);
}

mqd_t mqCreate(char* name, int maxMsgCnt, int maxMsgSz) {
    struct mq_attr mqAttrs;
    mqAttrs.mq_maxmsg = maxMsgCnt;
    mqAttrs.mq_msgsize = maxMsgSz;

    mqd_t queue = mq_open(name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR, &mqAttrs);
    if (queue == (mqd_t) -1) fail("cannot create queue");

    return queue;
}

mqd_t mqOpen(char* name) {
    mqd_t queue = mq_open(name, O_RDWR);
    if (queue == (mqd_t) -1) fail("cannot open queue");

    return queue;
}

void mqClose(mqd_t q) {
    mq_close(q);
}

void mqDispose(char* name) {
    mq_unlink(name);
}

void mqSend(mqd_t q, Message* msg, int prio) {
    if (mq_send(q, (const char*) msg, MSGSZ, prio) < 0) fail("send");
}

Message* mqReceive(mqd_t q, Message* msg, unsigned* prio) {
    if (mq_receive(q, (char*) msg, (size_t) MSGSZ, prio) < 0) fail("receive");
    return msg;
}

unsigned mqSize(mqd_t q) {
    struct mq_attr attrs;
    if (mq_getattr(q, &attrs) < 0) perror("mq_getattr");
    return (unsigned int) attrs.mq_curmsgs;
}

