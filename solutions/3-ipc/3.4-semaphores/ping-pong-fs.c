#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

void error(char* msg) { perror(msg); exit(2); }

sem_t*      openSemaphore(const char* name) {
    sem_t*  sem = sem_open(name, 0);
    if (sem == SEM_FAILED) {
        error("Cannot create SEM");
    }
    return sem;
}

sem_t*      newSemaphore(const char* name, int count) {
    sem_t*  sem = sem_open(name, O_CREAT | O_EXCL, S_IRUSR | S_IWUSR, count);
    if (sem == SEM_FAILED) {
        if (errno == EEXIST) return openSemaphore(name);
        error("Cannot create SEM");
    }
    return sem;
}

void    task(int numOperations, const char* msg, sem_t* my_turn, sem_t* your_turn) {
    int  k; for (k = 0; k < numOperations; ++k) {
        if (sem_wait(my_turn) != 0) error("Failed to invoke sem_wait");
            
        printf("%s (%d)\n", msg, k); 
        
        if (sem_post(your_turn) != 0) error("Failed to invoke sem_post");
    }
}

int main(int argc, char* argv[]) {
    int         n = 25; if (argc > 1) n = atoi(argv[1]);
    const char* semName1 = "/ping-pong-fs-1";
    const char* semName2 = "/ping-pong-fs-2";
    
    printf("Creating SEM 1\n");
    sem_t*  sem1 = newSemaphore(semName1, 0);
    
    if (fork() == 0) {
        printf("[PONG] started\n");
            
        printf("[PONG] Creating SEM 2\n");
        sem_t*  sem2 = newSemaphore(semName2, 1);
        if (sem_post(sem1) != 0) error("Failed to invoke sem_post");
        
        task(n, "             PONG", sem2, sem1);        
        sem_close(sem2);
        sem_close(sem1);
        exit(0);
    }
    
    {
        printf("[PING] started\n");
            
        if (sem_wait(sem1) != 0) error("Failed to invoke sem_wait");
        printf("[PING] Opening SEM 2\n");
        sem_t*  sem2 = openSemaphore(semName2);      
          
        task(n, "PING", sem1, sem2);         
        sem_close(sem2);
        sem_close(sem1);
    }  

    sem_unlink(semName2); 
    sem_unlink(semName1); 
    return 0;
}

