cmake_minimum_required(VERSION 3.7)
project(shared_memory)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wpedantic -Werror -Wfatal-errors  -Wno-pointer-arith")

add_executable(producer-consumer
        Thread.hpp
        Mutex.hpp
        Guard.hpp
        Condition.hpp
        MessageQueue.hpp
        SharedMemory.hpp
        producer-consumer.cpp
        )
target_link_libraries(producer-consumer pthread rt)

