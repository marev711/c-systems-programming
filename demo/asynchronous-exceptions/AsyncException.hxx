#pragma once

#include <string>
#include <initializer_list>
#include <set>
#include <csignal>
#include <cstring>
#include <algorithm>
#include "Trace.hxx"
using namespace std;

/**
 *  Thrown from a signal handler
 */
struct AsyncException {
    string type;
    AsyncException(const string& _type) : type(_type) {}
};


class AsyncExceptionHandler {
    static set<int> signals;

    /**
     *  Registers a UNIX signal-handler for the given signal.
     */
    void registerSignal(int signo, sighandler_t handler) {
        struct sigaction s;
        memset(&s, 0, sizeof(s));
        s.sa_handler = handler;
        s.sa_flags   = SA_NODEFER;
        if (sigaction(signo, &s, NULL) != 0)
            throw "Failed to register signal handler";

        sigset_t signals;
        sigemptyset(&signals);
        sigaddset(&signals, signo);
        if (sigprocmask(SIG_UNBLOCK, &signals, NULL) != 0)
            throw "Failed to setup signal";
    }

    /**
     *  Signal-handler that throws a C++ exception
     */
    static void throwCppException(int signo) {
        Trace t("throwCppException(signo=" + to_string(signo) + ")");

        if (signals.count(signo)) {
            throw AsyncException(strsignal(signo));
        }

        cerr << "Unexpected signal: " << signo << endl;
    }

public:
    AsyncExceptionHandler(initializer_list<int> trappedSignals) {
        for_each(trappedSignals.begin(), trappedSignals.end(), [&](int signo) {
            signals.insert(signo);
            registerSignal(signo, &throwCppException);
        });
    }

    ~AsyncExceptionHandler()                                       = default;
    AsyncExceptionHandler(const AsyncExceptionHandler&)            = delete;
    AsyncExceptionHandler& operator=(const AsyncExceptionHandler&) = delete;
};


